package com.hendisantika.ecommercebackend.controller;

import com.hendisantika.ecommercebackend.dto.Purchase;
import com.hendisantika.ecommercebackend.dto.PurchaseResponse;
import com.hendisantika.ecommercebackend.service.CheckoutService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : ecommerce
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/05/21
 * Time: 09.02
 */
@RestController
@CrossOrigin()
@RequestMapping("/api/checkout")
public class CheckoutController {
    private final CheckoutService checkoutService;

    public CheckoutController(CheckoutService checkoutService) {
        this.checkoutService = checkoutService;
    }

    @PostMapping("/purchase")
    public PurchaseResponse purchase(@RequestBody Purchase purchase) {
        PurchaseResponse response = this.checkoutService.placeOrder(purchase);
        return response;
    }
}

package com.hendisantika.ecommercebackend.dto;

import com.hendisantika.ecommercebackend.entity.Address;
import com.hendisantika.ecommercebackend.entity.Customer;
import com.hendisantika.ecommercebackend.entity.Order;
import com.hendisantika.ecommercebackend.entity.OrderItem;
import lombok.Data;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : ecommerce
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/05/21
 * Time: 08.43
 */
@Data
public class Purchase {
    private Customer customer;
    private Address shippingAddress;
    private Address billingAddress;
    private Order order;
    private List<OrderItem> orderItems;
}

package com.hendisantika.ecommercebackend.dto;

import lombok.Data;
import lombok.NonNull;

/**
 * Created by IntelliJ IDEA.
 * Project : ecommerce
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/05/21
 * Time: 08.44
 */
@Data
public class PurchaseResponse {

    @NonNull
    private String orderTrackingNumber;
}

package com.hendisantika.ecommercebackend.repository;

import com.hendisantika.ecommercebackend.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : ecommerce
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/05/21
 * Time: 08.47
 */
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}

export default {
    oidc: {
        clientId: '0oapmjdu0NyzFSGrq5d6',
        issuer: 'https://dev-26189778.okta.com/oauth2/default',
        redirectUri: 'http://localhost:4200/login/callback',
        scopes: ['openid','profile','email']
    }
}
